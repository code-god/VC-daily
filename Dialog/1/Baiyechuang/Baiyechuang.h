// Baiyechuang.h : main header file for the BAIYECHUANG application
//

#if !defined(AFX_BAIYECHUANG_H__01B4C5B7_7E99_40C7_9229_43BD757AD1A8__INCLUDED_)
#define AFX_BAIYECHUANG_H__01B4C5B7_7E99_40C7_9229_43BD757AD1A8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CBaiyechuangApp:
// See Baiyechuang.cpp for the implementation of this class
//

class CBaiyechuangApp : public CWinApp
{
public:
	CBaiyechuangApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaiyechuangApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CBaiyechuangApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAIYECHUANG_H__01B4C5B7_7E99_40C7_9229_43BD757AD1A8__INCLUDED_)
