// BaiyechuangDlg.h : header file
//

#if !defined(AFX_BAIYECHUANGDLG_H__D8EE8E1E_6086_4458_9790_678E6DC0C952__INCLUDED_)
#define AFX_BAIYECHUANGDLG_H__D8EE8E1E_6086_4458_9790_678E6DC0C952__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CBaiyechuangDlg dialog

class CBaiyechuangDlg : public CDialog
{
// Construction
public:
	CBaiyechuangDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CBaiyechuangDlg)
	enum { IDD = IDD_BAIYECHUANG_DIALOG };
	CString	m_Edit;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CBaiyechuangDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CBaiyechuangDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_BAIYECHUANGDLG_H__D8EE8E1E_6086_4458_9790_678E6DC0C952__INCLUDED_)
