// OpenDlgDlg.h : header file
//

#if !defined(AFX_OPENDLGDLG_H__95C19BC7_F8C3_4E28_A5CE_22AD32A7091A__INCLUDED_)
#define AFX_OPENDLGDLG_H__95C19BC7_F8C3_4E28_A5CE_22AD32A7091A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// COpenDlgDlg dialog

class COpenDlgDlg : public CDialog
{
// Construction
public:
	COpenDlgDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(COpenDlgDlg)
	enum { IDD = IDD_OPENDLG_DIALOG };
	CString	m_Edit;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(COpenDlgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(COpenDlgDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_OPENDLGDLG_H__95C19BC7_F8C3_4E28_A5CE_22AD32A7091A__INCLUDED_)
