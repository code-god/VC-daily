// newOpenDlg.h : main header file for the NEWOPENDLG application
//

#if !defined(AFX_NEWOPENDLG_H__5E39011E_57B4_4DD2_8FF5_8E0B96A131B6__INCLUDED_)
#define AFX_NEWOPENDLG_H__5E39011E_57B4_4DD2_8FF5_8E0B96A131B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNewOpenDlgApp:
// See newOpenDlg.cpp for the implementation of this class
//

class CNewOpenDlgApp : public CWinApp
{
public:
	CNewOpenDlgApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewOpenDlgApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNewOpenDlgApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWOPENDLG_H__5E39011E_57B4_4DD2_8FF5_8E0B96A131B6__INCLUDED_)
