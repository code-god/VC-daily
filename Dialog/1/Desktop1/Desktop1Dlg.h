// Desktop1Dlg.h : header file
//

#if !defined(AFX_DESKTOP1DLG_H__2DEACDF5_2E52_43FA_91F3_614A90304F9F__INCLUDED_)
#define AFX_DESKTOP1DLG_H__2DEACDF5_2E52_43FA_91F3_614A90304F9F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CDesktop1Dlg dialog

class CDesktop1Dlg : public CDialog
{
// Construction
public:
	CDesktop1Dlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CDesktop1Dlg)
	enum { IDD = IDD_DESKTOP1_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDesktop1Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CDesktop1Dlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnMove(int x, int y);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DESKTOP1DLG_H__2DEACDF5_2E52_43FA_91F3_614A90304F9F__INCLUDED_)
