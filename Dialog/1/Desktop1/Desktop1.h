// Desktop1.h : main header file for the DESKTOP1 application
//

#if !defined(AFX_DESKTOP1_H__70ED9258_7F1F_4A6A_8BB7_92C315205302__INCLUDED_)
#define AFX_DESKTOP1_H__70ED9258_7F1F_4A6A_8BB7_92C315205302__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CDesktop1App:
// See Desktop1.cpp for the implementation of this class
//

class CDesktop1App : public CWinApp
{
public:
	CDesktop1App();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDesktop1App)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CDesktop1App)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DESKTOP1_H__70ED9258_7F1F_4A6A_8BB7_92C315205302__INCLUDED_)
