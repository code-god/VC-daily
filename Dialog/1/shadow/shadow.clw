; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDialog0
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "shadow.h"

ClassCount=4
Class1=CShadowApp
Class2=CShadowDlg
Class3=CAboutDlg

ResourceCount=5
Resource1=IDD_SHADOW_DIALOG
Resource2=IDR_MAINFRAME
Resource3=IDD_DIALOG1
Class4=CDialog0
Resource4=IDD_ABOUTBOX
Resource5=IDD_DIALOG2

[CLS:CShadowApp]
Type=0
HeaderFile=shadow.h
ImplementationFile=shadow.cpp
Filter=N

[CLS:CShadowDlg]
Type=0
HeaderFile=shadowDlg.h
ImplementationFile=shadowDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC

[CLS:CAboutDlg]
Type=0
HeaderFile=shadowDlg.h
ImplementationFile=shadowDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_SHADOW_DIALOG]
Type=1
Class=CShadowDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352

[DLG:IDD_DIALOG1]
Type=1
Class=CDialog0
ControlCount=0

[CLS:CDialog0]
Type=0
HeaderFile=Dialog0.h
ImplementationFile=Dialog0.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC

[DLG:IDD_DIALOG2]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816

