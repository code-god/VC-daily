// shadow.h : main header file for the SHADOW application
//

#if !defined(AFX_SHADOW_H__BC48BFD3_9EAA_41CF_862C_341E89A9AA09__INCLUDED_)
#define AFX_SHADOW_H__BC48BFD3_9EAA_41CF_862C_341E89A9AA09__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CShadowApp:
// See shadow.cpp for the implementation of this class
//

class CShadowApp : public CWinApp
{
public:
	CShadowApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShadowApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CShadowApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHADOW_H__BC48BFD3_9EAA_41CF_862C_341E89A9AA09__INCLUDED_)
