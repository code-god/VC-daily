// shadowDlg.h : header file
//

#if !defined(AFX_SHADOWDLG_H__75F2CB23_9EAD_4AA2_AF30_77D991514280__INCLUDED_)
#define AFX_SHADOWDLG_H__75F2CB23_9EAD_4AA2_AF30_77D991514280__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CShadowDlg dialog
#include "Dialog0.h"
#include "Dialog1.h"
class CShadowDlg : public CDialog
{
// Construction
public:
	CShadowDlg(CWnd* pParent = NULL);	// standard constructor
	CDialog0 dlg1;
	CDialog1 dlg2;
// Dialog Data
	//{{AFX_DATA(CShadowDlg)
	enum { IDD = IDD_SHADOW_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CShadowDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CShadowDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnMove(int x, int y);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SHADOWDLG_H__75F2CB23_9EAD_4AA2_AF30_77D991514280__INCLUDED_)
