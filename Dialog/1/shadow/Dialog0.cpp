// Dialog0.cpp : implementation file
//

#include "stdafx.h"
#include "shadow.h"
#include "Dialog0.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDialog0 dialog


CDialog0::CDialog0(CWnd* pParent /*=NULL*/)
	: CDialog(CDialog0::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDialog0)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDialog0::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDialog0)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDialog0, CDialog)
	//{{AFX_MSG_MAP(CDialog0)
	ON_WM_CREATE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDialog0 message handlers

int CDialog0::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	// TODO: Add your specialized creation code here
	SetWindowLong(GetSafeHwnd(),GWL_EXSTYLE,GetWindowLong(GetSafeHwnd(),GWL_EXSTYLE)|0x80000);
	typedef BOOL(WINAPI *FSetLayeredWindowAttributes)(HWND,COLORREF,BYTE,DWORD);
	FSetLayeredWindowAttributes SetLayeredWindowAttributes;
	HINSTANCE hInst=LoadLibrary("User32.DLL");
	SetLayeredWindowAttributes=(FSetLayeredWindowAttributes)
	GetProcAddress(hInst,"SetLayeredWindowAttributes");
	if(SetLayeredWindowAttributes)SetLayeredWindowAttributes(GetSafeHwnd(),RGB(0,0,0),128,1);
	FreeLibrary(hInst);
	return 0;
}
