#if !defined(AFX_DIALOG0_H__E88BE427_4DAE_4B60_9BBE_7E91AA0AE57A__INCLUDED_)
#define AFX_DIALOG0_H__E88BE427_4DAE_4B60_9BBE_7E91AA0AE57A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Dialog0.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDialog0 dialog

class CDialog0 : public CDialog
{
// Construction
public:
	CDialog0(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDialog0)
	enum { IDD = IDD_DIALOG1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDialog0)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDialog0)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DIALOG0_H__E88BE427_4DAE_4B60_9BBE_7E91AA0AE57A__INCLUDED_)
