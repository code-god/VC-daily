// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//
#undef WINVER
#define WINVER 0X500
#if _MSC_VER > 1000
#if !defined(AFX_STDAFX_H__6C3C81F0_601E_4466_BEFC_ABDDA5FDEB3C__INCLUDED_)
#define AFX_STDAFX_H__6C3C81F0_601E_4466_BEFC_ABDDA5FDEB3C__INCLUDED_



#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdisp.h>        // MFC Automation classes
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__6C3C81F0_601E_4466_BEFC_ABDDA5FDEB3C__INCLUDED_)
