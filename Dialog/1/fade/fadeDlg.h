// fadeDlg.h : header file
//

#if !defined(AFX_FADEDLG_H__D6D4F436_9672_44A9_97EB_EA39AAB02D1A__INCLUDED_)
#define AFX_FADEDLG_H__D6D4F436_9672_44A9_97EB_EA39AAB02D1A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CFadeDlg dialog

class CFadeDlg : public CDialog
{
// Construction
public:
	CFadeDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CFadeDlg)
	enum { IDD = IDD_FADE_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFadeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CFadeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FADEDLG_H__D6D4F436_9672_44A9_97EB_EA39AAB02D1A__INCLUDED_)
