// fade.h : main header file for the FADE application
//

#if !defined(AFX_FADE_H__73A6B6F3_6384_4CDF_AC9D_11252D2C9AB4__INCLUDED_)
#define AFX_FADE_H__73A6B6F3_6384_4CDF_AC9D_11252D2C9AB4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFadeApp:
// See fade.cpp for the implementation of this class
//

class CFadeApp : public CWinApp
{
public:
	CFadeApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFadeApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFadeApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FADE_H__73A6B6F3_6384_4CDF_AC9D_11252D2C9AB4__INCLUDED_)
