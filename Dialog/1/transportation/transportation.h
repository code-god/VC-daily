// transportation.h : main header file for the TRANSPORTATION application
//

#if !defined(AFX_TRANSPORTATION_H__9657EE24_0635_4926_A1D0_D579FFCFB8C9__INCLUDED_)
#define AFX_TRANSPORTATION_H__9657EE24_0635_4926_A1D0_D579FFCFB8C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTransportationApp:
// See transportation.cpp for the implementation of this class
//

class CTransportationApp : public CWinApp
{
public:
	CTransportationApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTransportationApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTransportationApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSPORTATION_H__9657EE24_0635_4926_A1D0_D579FFCFB8C9__INCLUDED_)
