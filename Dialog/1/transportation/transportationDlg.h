// transportationDlg.h : header file
//

#if !defined(AFX_TRANSPORTATIONDLG_H__B483587A_9B5A_4F41_9544_06FC5525E0DC__INCLUDED_)
#define AFX_TRANSPORTATIONDLG_H__B483587A_9B5A_4F41_9544_06FC5525E0DC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CTransportationDlg dialog

class CTransportationDlg : public CDialog
{
// Construction
public:
	CTransportationDlg(CWnd* pParent = NULL);	// standard constructor
	CFont m_font;
// Dialog Data
	//{{AFX_DATA(CTransportationDlg)
	enum { IDD = IDD_TRANSPORTATION_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTransportationDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CTransportationDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSPORTATIONDLG_H__B483587A_9B5A_4F41_9544_06FC5525E0DC__INCLUDED_)
