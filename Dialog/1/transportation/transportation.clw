; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CTransportationDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "transportation.h"

ClassCount=4
Class1=CTransportationApp
Class2=CTransportationDlg
Class3=CAboutDlg

ResourceCount=3
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Resource3=IDD_TRANSPORTATION_DIALOG

[CLS:CTransportationApp]
Type=0
HeaderFile=transportation.h
ImplementationFile=transportation.cpp
Filter=N

[CLS:CTransportationDlg]
Type=0
HeaderFile=transportationDlg.h
ImplementationFile=transportationDlg.cpp
Filter=D

[CLS:CAboutDlg]
Type=0
HeaderFile=transportationDlg.h
ImplementationFile=transportationDlg.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_TRANSPORTATION_DIALOG]
Type=1
Class=CTransportationDlg
ControlCount=3
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816
Control3=IDC_STATIC,static,1342308352

