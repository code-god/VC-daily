// SaveTxtDlg.h : main header file for the SAVETXTDLG application
//

#if !defined(AFX_SAVETXTDLG_H__93A93D41_485C_4C28_B54D_04BAF8E8C0DE__INCLUDED_)
#define AFX_SAVETXTDLG_H__93A93D41_485C_4C28_B54D_04BAF8E8C0DE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CSaveTxtDlgApp:
// See SaveTxtDlg.cpp for the implementation of this class
//

class CSaveTxtDlgApp : public CWinApp
{
public:
	CSaveTxtDlgApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveTxtDlgApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CSaveTxtDlgApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVETXTDLG_H__93A93D41_485C_4C28_B54D_04BAF8E8C0DE__INCLUDED_)
