// SaveTxtDlgDlg.h : header file
//

#if !defined(AFX_SAVETXTDLGDLG_H__C5F6DD51_45D6_4642_95FE_A0012FC5FEF9__INCLUDED_)
#define AFX_SAVETXTDLGDLG_H__C5F6DD51_45D6_4642_95FE_A0012FC5FEF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSaveTxtDlgDlg dialog

class CSaveTxtDlgDlg : public CDialog
{
// Construction
public:
	CSaveTxtDlgDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CSaveTxtDlgDlg)
	enum { IDD = IDD_SAVETXTDLG_DIALOG };
	CString	m_Edit;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSaveTxtDlgDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CSaveTxtDlgDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnButton1();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SAVETXTDLGDLG_H__C5F6DD51_45D6_4642_95FE_A0012FC5FEF9__INCLUDED_)
