// RadiusWindow.h : main header file for the RADIUSWINDOW application
//

#if !defined(AFX_RADIUSWINDOW_H__2A1F4A1D_3496_44F8_B2C5_B4D6AD1221BC__INCLUDED_)
#define AFX_RADIUSWINDOW_H__2A1F4A1D_3496_44F8_B2C5_B4D6AD1221BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CRadiusWindowApp:
// See RadiusWindow.cpp for the implementation of this class
//

class CRadiusWindowApp : public CWinApp
{
public:
	CRadiusWindowApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRadiusWindowApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CRadiusWindowApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RADIUSWINDOW_H__2A1F4A1D_3496_44F8_B2C5_B4D6AD1221BC__INCLUDED_)
