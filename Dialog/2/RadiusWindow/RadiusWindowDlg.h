// RadiusWindowDlg.h : header file
//

#if !defined(AFX_RADIUSWINDOWDLG_H__E7F7FC0D_F2D5_4C3F_8787_08650B3D06B6__INCLUDED_)
#define AFX_RADIUSWINDOWDLG_H__E7F7FC0D_F2D5_4C3F_8787_08650B3D06B6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CRadiusWindowDlg dialog

class CRadiusWindowDlg : public CDialog
{
// Construction
public:
	CRadiusWindowDlg(CWnd* pParent = NULL);	// standard constructor
	CFont font;
// Dialog Data
	//{{AFX_DATA(CRadiusWindowDlg)
	enum { IDD = IDD_RADIUSWINDOW_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRadiusWindowDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CRadiusWindowDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RADIUSWINDOWDLG_H__E7F7FC0D_F2D5_4C3F_8787_08650B3D06B6__INCLUDED_)
