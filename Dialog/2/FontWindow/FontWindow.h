// FontWindow.h : main header file for the FONTWINDOW application
//

#if !defined(AFX_FONTWINDOW_H__BFA45D05_22F4_4CCD_BF73_F8E974CA0239__INCLUDED_)
#define AFX_FONTWINDOW_H__BFA45D05_22F4_4CCD_BF73_F8E974CA0239__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CFontWindowApp:
// See FontWindow.cpp for the implementation of this class
//

class CFontWindowApp : public CWinApp
{
public:
	CFontWindowApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFontWindowApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CFontWindowApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FONTWINDOW_H__BFA45D05_22F4_4CCD_BF73_F8E974CA0239__INCLUDED_)
