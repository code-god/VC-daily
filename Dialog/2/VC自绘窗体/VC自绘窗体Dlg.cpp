// VC自绘窗体Dlg.cpp : implementation file
//

#include "stdafx.h"
#include "VC自绘窗体.h"
#include "VC自绘窗体Dlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVCDlg dialog

CVCDlg::CVCDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVCDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CVCDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_Ptbrush.CreateSolidBrush(RGB(255,255,255));
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CVCDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVCDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CVCDlg, CDialog)
	//{{AFX_MSG_MAP(CVCDlg)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_NCMOUSEMOVE()
	ON_WM_NCPAINT()
	ON_WM_SIZE()
	ON_WM_ACTIVATE()
	ON_WM_NCACTIVATE()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVCDlg message handlers

BOOL CVCDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	 
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CVCDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CVCDlg::OnPaint() 
{
	DrawDialog(LEFTTITLE);
	DrawDialog(MIDTITLE);
	DrawDialog(RIGHTTITLE);
	DrawDialog(LEFTBAR);
	DrawDialog(RIGHTBAR);
	DrawDialog(BOTTOMBAR);

	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CVCDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CVCDlg::OnNcMouseMove(UINT nHitTest, CPoint point) 
{
	// TODO: Add your message handler code here and/or call default
	CRect MinRC,MAXRC,CloseRC,WINRC;
    CWindowDC WindowDC(this);
    CDC memDC;
    memDC.CreateCompatibleDC(&WindowDC);
    BITMAPINFO bmpinfo;
    CBitmap Bmp;
    int nBmpCX,nBmpCY;
    GetWindowRect(WINRC);
    MinRC.CopyRect(CRect(WINRC.left+m_MinRC.left,WINRC.top+m_MinRC.top,WINRC.left+m_MinRC.right,WINRC.top+m_MinRC.bottom));
    MAXRC.CopyRect(CRect(WINRC.left+m_MAXRC.left,WINRC.top+m_MAXRC.top,WINRC.left+m_MAXRC.right,WINRC.top+m_MAXRC.bottom));
    CloseRC.CopyRect(CRect(WINRC.left+m_CloseRC.left,WINRC.top+m_CloseRC.top,WINRC.left+m_CloseRC.right,WINRC.top+m_CloseRC.bottom));

	if (MinRC.PtInRect(point))
    {
        if (m_BtnState!=BS_MIN)
        {
            MessageBox("ddd");
        }
    }
	CDialog::OnNcMouseMove(nHitTest, point);
}

int CVCDlg::DrawDialog(int nFlag)
{
	int nFrameCY=GetSystemMetrics(SM_CYFIXEDFRAME);
    int nFrameCX=GetSystemMetrics(SM_CXFIXEDFRAME);
	int m_nBorderCX,m_nBorderCY,m_nTitleBarCY,m_nRightTitleCX;
    if (GetStyle()&WS_BORDER)
    {
        m_nBorderCY=nFrameCY+2*GetSystemMetrics(SM_CYBORDER)+2*GetSystemMetrics(SM_CYEDGE);
        m_nBorderCX=nFrameCX+2*GetSystemMetrics(SM_CXBORDER)+2*GetSystemMetrics(SM_CXEDGE);
    }
    else
    {
        m_nBorderCX=nFrameCX;
        m_nBorderCY=nFrameCY;
    }
    m_nTitleBarCY=GetSystemMetrics(SM_CYCAPTION)+m_nBorderCY;
    CRect ClientRC;
    GetClientRect(ClientRC);
    CRect WinRC,FactRC;
    GetWindowRect(WinRC);
    FactRC.CopyRect(CRect(0,0,WinRC.Width(),WinRC.Height()));
    CWindowDC WindowDC(this);
    CBitmap Bmp;
    BITMAPINFO bmpinfo;
    CDC memDC;
    memDC.CreateCompatibleDC(&WindowDC);
    if (nFlag&LEFTBAR)
    {
        Bmp.LoadBitmap(IDB_BITMAP_LEFTFORM);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(0,m_nTitleBarCY,m_nBorderCX,FactRC.Height()-m_nTitleBarCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
    //
    int nLeftBmpCX=0;
    int nRightBmpCX=0;
    if (nFlag&LEFTTITLE)
    {
        Bmp.LoadBitmap(IDB_BITMAP_UPLEFT);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        nLeftBmpCX=nBmpCX;
        WindowDC.StretchBlt(0,0,nBmpCX,m_nTitleBarCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
    if (nFlag&RIGHTTITLE)
    {
        Bmp.LoadBitmap(IDB_BITMAP_UPRIGHT);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        nRightBmpCX=nBmpCX;
        m_nRightTitleCX=nRightBmpCX;
        int nOrgX=FactRC.Width()-nBmpCX;
        m_TitleBarRC.CopyRect(CRect(FactRC.Width()-nBmpCX,0,FactRC.right,m_nTitleBarCY));
        WindowDC.StretchBlt(nOrgX,0,nBmpCX,m_nTitleBarCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
    if (nFlag&MIDTITLE)
    {
        Bmp.LoadBitmap(IDB_BITMAP_UPMID);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        int nMidStreatch=FactRC.Width()-nLeftBmpCX-nRightBmpCX;
        WindowDC.StretchBlt(nLeftBmpCX,0,nMidStreatch,m_nTitleBarCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
		WindowDC.SetBkMode(TRANSPARENT);
		WindowDC.TextOut(nLeftBmpCX-38,8,"VC自绘窗体");
		
        Bmp.DeleteObject();
    }
    if (nFlag&RIGHTBAR)
    {
        Bmp.LoadBitmap(IDB_BITMAP_RIGHTFORM);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(FactRC.Width()-m_nBorderCX,m_nTitleBarCY,m_nBorderCX,FactRC.Height()-m_nTitleBarCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
	
    if (nFlag&BOTTOMBAR)
    {
        Bmp.LoadBitmap(IDB_BITMAP_BOTTOMFORM);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(m_nBorderCX,FactRC.Height()-m_nBorderCY,FactRC.Width()-2*m_nBorderCX,m_nBorderCY,&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
	/*
    if (nFlag&MINBUTTON)
    {
        Bmp.LoadBitmap(IDB_BITMAP_MIN);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(m_MinRC.left,m_MinRC.top,m_MinRC.Width(),m_MinRC.Height(),&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
    if (nFlag&MAXBUTTON)
    {
        Bmp.LoadBitmap(IDB_BITMAP_MAX);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(m_MAXRC.left,m_MAXRC.top,m_MAXRC.Width(),m_MAXRC.Height(),&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
    if (nFlag&CLOSEBUTTON)
    {
        Bmp.LoadBitmap(IDB_BITMAP_CLOSE);
        memDC.SelectObject(&Bmp);
        Bmp.GetObject(sizeof(BITMAPINFO),&bmpinfo);
        int nBmpCX=bmpinfo.bmiHeader.biWidth;
        int nBmpCY=bmpinfo.bmiHeader.biHeight;
        WindowDC.StretchBlt(m_CloseRC.left,m_CloseRC.top,m_CloseRC.Width(),m_CloseRC.Height(),&memDC,0,0,nBmpCX,nBmpCY,SRCCOPY);
        Bmp.DeleteObject();
    }
	*/
    ReleaseDC(&memDC);
    return 0;
}

void CVCDlg::OnNcPaint() 
{
	// TODO: Add your message handler code here
	

	// Do not call CDialog::OnNcPaint() for painting messages
}

void CVCDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);
	//获取窗口边框的高度
    //获取窗口边框的宽度
    //根据窗口的风格计算对话框的高度和宽度
    //获取对话框是否有边框
    //计算标题栏宽度
    //获取客户区域
    //获取窗口区域
    //更新整个窗口
    int nFrameCY = GetSystemMetrics(SM_CYFIXEDFRAME);
    int nFrameCX = GetSystemMetrics(SM_CXFIXEDFRAME);
    //希望能够调试输出信息
	int m_nBorderCX,m_nBorderCY,m_nTitleBarCY,m_nRightTitleCX;
    if (GetStyle()&WS_BORDER)
    {
        m_nBorderCY = nFrameCY+2*GetSystemMetrics(SM_CYBORDER)+2*GetSystemMetrics(SM_CYEDGE);
        m_nBorderCX = nFrameCX+2*GetSystemMetrics(SM_CXBORDER)+2*GetSystemMetrics(SM_CXEDGE);
    }
    else
    {
        m_nBorderCY=nFrameCY;
        m_nBorderCX=nFrameCX;
    }
    //
    m_nTitleBarCY = GetSystemMetrics(SM_CYCAPTION)+m_nBorderCY; //标题栏高度
    CRect ClientRC;
    GetClientRect(ClientRC);
    CRect WinRC;
    GetWindowRect(WinRC);                                  //为什么不是地址？
    //
	/*
	m_MinRC.left=m_MinPoint.x+WinRC.Width()-m_nRightTitleCX;    //逻辑坐标？
    m_MinRC.top=(m_nTitleBarCY-m_nTitleBtnCY)/2+m_MinPoint.y; 
    m_MinRC.right=m_nTitleBtnCX+m_MinRC.left;
    m_MinRC.bottom=m_MinRC.top+m_nTitleBtnCY;

    m_MAXRC.left=m_MAXPoint.x+WinRC.Width()-m_nRightTitleCX;    //逻辑坐标？
    m_MAXRC.top=(m_nTitleBarCY-m_nTitleBtnCY)/2+m_MAXPoint.y;
    m_MAXRC.right=m_nTitleBtnCX+m_MAXRC.left;
    m_MAXRC.bottom=m_MAXRC.top+m_nTitleBtnCY;

    m_CloseRC.left=m_ClosePoint.x+WinRC.Width()-m_nRightTitleCX;    //逻辑坐标？
    m_CloseRC.top=(m_nTitleBarCY-m_nTitleBtnCY)/2+m_ClosePoint.y;
    m_CloseRC.right=m_nTitleBtnCX+m_CloseRC.left;
    m_CloseRC.bottom=m_CloseRC.top+m_nTitleBtnCY;
*/
//    m_WinRCWidth=WinRC.Width();

    Invalidate();                                         //强制更新;
	// TODO: Add your message handler code here
	
}

void CVCDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);
	Invalidate();
	// TODO: Add your message handler code here
	
}

BOOL CVCDlg::OnNcActivate(BOOL bActive) 
{
	// TODO: Add your message handler code here and/or call default
	Invalidate();
	return CDialog::OnNcActivate(bActive);
}

HBRUSH CVCDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr;
	COLORREF m_crBK= RGB(255,255,255);
	if(pWnd->GetDlgCtrlID()==IDC_BUTTON1){
	}
    if (nCtlColor==CTLCOLOR_DLG)
    {
        CBrush m_Brush(m_crBK);
        CRect rect;
        GetClientRect(rect);
        pDC->SelectObject(&m_Brush);
        pDC->FillRect(rect,&m_Brush);
        return m_Brush;
    }
    else
        hbr=CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	
	// TODO: Change any attributes of the DC here
	
	// TODO: Return a different brush if the default is not desired
	return hbr;
}
