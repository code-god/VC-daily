// VC�Ի洰��Dlg.h : header file
//

#if !defined(AFX_VCDLG_H__CEF46163_9049_4070_990D_4749311FE09A__INCLUDED_)
#define AFX_VCDLG_H__CEF46163_9049_4070_990D_4749311FE09A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CVCDlg dialog
#define BS_MIN 100
#define LEFTBAR 11
#define LEFTTITLE 12
#define RIGHTTITLE 13
#define MIDTITLE 14
#define BOTTOMBAR 15
#define MINBUTTON 16
#define CLOSEBUTTON 17
#define RIGHTBAR 18
class CVCDlg : public CDialog
{
// Construction
public:
	int DrawDialog(int nFlag);
	CVCDlg(CWnd* pParent = NULL);	// standard constructor
	CRect m_MinRC,m_MAXRC,m_CloseRC,m_TitleBarRC,m_WinRCWidth;
	int m_BtnState;
	CBrush  m_Ptbrush;
// Dialog Data
	//{{AFX_DATA(CVCDlg)
	enum { IDD = IDD_VC_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVCDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CVCDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnNcMouseMove(UINT nHitTest, CPoint point);
	afx_msg void OnNcPaint();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg BOOL OnNcActivate(BOOL bActive);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VCDLG_H__CEF46163_9049_4070_990D_4749311FE09A__INCLUDED_)
