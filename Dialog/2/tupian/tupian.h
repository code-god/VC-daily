// tupian.h : main header file for the TUPIAN application
//

#if !defined(AFX_TUPIAN_H__DE9CE59D_717E_437B_9D55_23EB4CD8F8ED__INCLUDED_)
#define AFX_TUPIAN_H__DE9CE59D_717E_437B_9D55_23EB4CD8F8ED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CTupianApp:
// See tupian.cpp for the implementation of this class
//

class CTupianApp : public CWinApp
{
public:
	CTupianApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTupianApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTupianApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TUPIAN_H__DE9CE59D_717E_437B_9D55_23EB4CD8F8ED__INCLUDED_)
